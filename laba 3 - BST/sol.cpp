#pragma once
#include <algorithm>
#include <iostream>
#include <set>

namespace Solution {

struct Treap {
  std::set<int> s;
};

Treap* Construct(Treap* temp) { return new Treap;}
bool Find(Treap* t, int v) { return t->s.find(v) != t->s.end(); }
void Insert(Treap* t, int v) { t->s.insert(v); }
void Erase(Treap* t, int v) { t->s.erase(v); }


struct AVLTree {
  std::set<int> s;
};

AVLTree* Construct(AVLTree* temp) { return new AVLTree;}
bool Find(AVLTree* t, int v) { return t->s.find(v) != t->s.end(); }
void Insert(AVLTree* t, int v) { t->s.insert(v); }
void Erase(AVLTree* t, int v) { t->s.erase(v); }


struct SplayTree {
  std::set<int> s;
};

SplayTree* Construct(SplayTree* temp) { return new SplayTree;}
bool Find(SplayTree* t, int v) { return t->s.find(v) != t->s.end(); }
void Insert(SplayTree* t, int v) { t->s.insert(v); }
void Erase(SplayTree* t, int v) { t->s.erase(v); }


struct BTree {
  std::set<int> s;
};

BTree* Construct(BTree* temp) { return new BTree;}
bool Find(BTree* t, int v) { return t->s.find(v) != t->s.end(); }
void Insert(BTree* t, int v) { t->s.insert(v); }
void Erase(BTree* t, int v) { t->s.erase(v); }


struct RBTree {
  std::set<int> s;
};

RBTree* Construct(RBTree* temp) { return new RBTree;}
bool Find(RBTree* t, int v) { return t->s.find(v) != t->s.end(); }
void Insert(RBTree* t, int v) { t->s.insert(v); }
void Erase(RBTree* t, int v) { t->s.erase(v); }

}  // namespace Solution
