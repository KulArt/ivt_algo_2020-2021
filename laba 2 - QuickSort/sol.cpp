#pragma once
#include <algorithm>
#include <iostream>

namespace Solution {
void QuickSort(int* begin, int* end) { std::sort(begin, end); }

void IntroQuickSort(int* begin, int* end) { std::sort(begin, end); }

void RandomQuickSort(int* begin, int* end) { std::sort(begin, end); }

void MedianQuickSort(int* begin, int* end) { std::sort(begin, end); }

void TripleQuickSort(int* begin, int* end) { std::sort(begin, end); }

void KTripleQuickSort(int* begin, int* end) { std::sort(begin, end); }

void MedianOfMediansQuickSort(int* begin, int* end) { std::sort(begin, end); }

void MedianIntroQuickSort(int* begin, int* end) { std::sort(begin, end); }

void TripleIntroQuickSort(int* begin, int* end) { std::sort(begin, end); }

void KTripleIntroQuickSort(int* begin, int* end) { std::sort(begin, end); }

void MedianOfMediansIntroQuickSort(int* begin, int* end) {
  std::sort(begin, end);
}
}  // namespace Solution

