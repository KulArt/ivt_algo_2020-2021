#include <ctime>
#include <functional>
#include <iostream>
#include <random>
#include <string>
#include <unordered_set>
#include <vector>

#include "sol.cpp"

class Tester {
 public:
  Tester() = default;
  Tester(const Tester&) = delete;
  Tester& operator=(const Tester&) = delete;
  Tester(Tester&&) = delete;
  Tester& operator=(Tester&&) = delete;

  void Test();

 private:
  using Array = std::vector<int>;
  const static size_t array_size_ = static_cast<size_t>(2e6);

  std::vector<Array> test_arrays_ = {
      GenerateArray(RAND, array_size_),
      GenerateArray(FIFTH, array_size_),
      GenerateArray(THREE, array_size_),
      GenerateArray(SAME, array_size_)
  };

  std::vector<std::function<void(int*, int*)>> test_functions_ = {
      Solution::QuickSort,
      Solution::IntroQuickSort,
      Solution::RandomQuickSort,
      Solution::MedianQuickSort,
      Solution::TripleQuickSort,
      Solution::KTripleQuickSort,
      Solution::MedianOfMediansQuickSort,
      Solution::MedianIntroQuickSort,
      Solution::TripleIntroQuickSort,
      Solution::KTripleIntroQuickSort,
      Solution::MedianOfMediansIntroQuickSort
  };

  int* user_array_ = nullptr;
  static const int seed_ = 32;

  std::function<void(int*, int*)> sort_algo_;

  enum TestType { RAND = 1, FIFTH, THREE, SAME };

  static void SuccessLog(const std::string& test_name, const size_t time);
  static void FailedLog(const std::string& test_name);
  void Log(const std::string& test_name, const size_t time);
  Array GenerateArray(const TestType type, const size_t size);
  static Array GenerateRand(const size_t size);
  static Array GenerateSet(const size_t array_size, const size_t set_size);
  static bool IsCorrect(const Array& result);
  static void Copy(int*& array, const Array& vec);
  static Array MakeVector(const int* begin, const int* end);

  void PrimeTest(const std::string& test_name,
                 const std::function<void(int*, int*)>& function,
                 const std::vector<int>& vec);
};

void Tester::SuccessLog(const std::string& test_name, const size_t time) {
  std::cout << "Test " << test_name
            << "time: " << std::round((time + 0.0) / CLOCKS_PER_SEC * 1000)
            << " milliseconds\n";
}

void Tester::FailedLog(const std::string& test_name) {
  std::cout << "Test " << test_name << "failed\n";
}

void Tester::Log(const std::string& test_name, const size_t time) {
  if (IsCorrect(MakeVector(user_array_, user_array_ + array_size_))) {
    SuccessLog(test_name, time);
  } else {
    FailedLog(test_name);
  }
}

Tester::Array Tester::GenerateArray(const TestType type, const size_t size) {
  std::vector<int> array;
  switch (type) {
    case RAND:
      return GenerateRand(size);
    case FIFTH:
      return GenerateSet(size, size / 100);
    case THREE:
      return GenerateSet(size, 3);
    case SAME:
      return GenerateSet(size, 1);
  }
}

Tester::Array Tester::GenerateRand(const size_t size) {
  std::mt19937_64 generator(seed_);
  std::vector<int> array(size, 0);
  std::uniform_int_distribution<size_t> distribution(0, array_size_);
  for (auto& elem : array) {
    elem = distribution(generator);
  }
  return array;
}

Tester::Array Tester::GenerateSet(const size_t array_size,
                                  const size_t set_size) {
  std::mt19937_64 generator(seed_);
  std::vector<int> array;
  array.reserve(array_size);
  std::uniform_int_distribution<size_t> distribution(0, array_size_);
  std::unordered_set<int> set;
  while (set.size() != set_size) {
    set.insert(distribution(generator));
  }

  while (array.size() < array_size) {
    for (const auto& elem : set) {
      array.push_back(elem);
    }
  }

  array.resize(array_size);
  std::shuffle(array.begin(), array.end(), generator);
  return array;
}

bool Tester::IsCorrect(const Array& result) {
  auto ans = result;
  std::sort(ans.begin(), ans.end());
  return result == ans;
}

void Tester::Copy(int*& array, const Array& vec) {
  array = new int[vec.size()];
  for (int i = 0; i < vec.size(); ++i) {
    array[i] = vec[i];
  }
}

Tester::Array Tester::MakeVector(const int* begin, const int* end) {
  Array result;
  for (auto it = begin; it != end; ++it) {
    result.push_back(*it);
  }
  return result;
}

void Tester::PrimeTest(const std::string& test_name,
                       const std::function<void(int*, int*)>& function,
                       const std::vector<int>& vec) {
  clock_t start, end;
  sort_algo_ = function;
  Copy(user_array_, vec);
  start = clock();
  sort_algo_(user_array_, user_array_ + array_size_);
  end = clock();
  Log(test_name, end - start);
}

void Tester::Test() {
  std::vector<std::string> algo_names = {
      "simple sort, ",
      "intro sort, ",
      "random pivot sort, ",
      "median pivot sort, ",
      "triple partition sort, ",
      "k-th order triple partition sort, ",
      "median of medians sort, ",
      "median of medians + Insertions sort, ",
      "triple partition + Insertions sort, ",
      "k-th order triple partition + Insertions sort, ",
      "median of medians + Insertions sort, "
  };
  std::vector<std::string> test_names = {
      "random elements array ",
      "1% unique elements array ",
      "three unique elements array ",
      "same elements array "
  };

  for (int i = 0; i < test_functions_.size(); ++i) {
    for (int j = 0; j < test_arrays_.size(); ++j) {
      auto name = algo_names[i] + test_names[j];
      PrimeTest(name, test_functions_[i], test_arrays_[j]);
    }
    std::cout << '\n';
  }
}

int main() {
  Tester tester;
  tester.Test();
}